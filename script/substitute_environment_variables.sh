#!/bin/sh

ROOT_DIR=/app/dist
# Replace env vars in files served by NGINX
for file in $ROOT_DIR/assets/*.js* $ROOT_DIR/index.html $ROOT_DIR/precache-manifest*.js;
do
  sed -i 's|VITE_PUBLIC_PATH_PLACEHOLDER|'${VITE_PUBLIC_PATH}'|g' $file
done

# Let container execution proceed
exec "$@"