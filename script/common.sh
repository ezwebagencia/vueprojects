export WORKDIR="$(dirname "${BASH_SOURCE[0]}")/.."
export DOCKER_COMPOSE="docker-compose -f $WORKDIR/docker-compose.yml"

export TOKEN=`grep 'password=' -m 1 ~/.npmrc | cut -d=   -f2`
export USERNAME=`grep 'username=' -m 1 ~/.npmrc | cut -d=  -f2`
export EMAIL=`grep 'email=' -m 1 ~/.npmrc | cut -d=   -f2`
export USER_ID=`id -u`

check_environment() {
  if [[ -z $TOKEN || -z $EMAIL ]]; then
    echo "Missing NPM repository credentials, please verify if your .npmrc is valid. Check: 'https://dev.azure.com/locaweb/apps-design-system/_wiki/wikis/apps-design-system.wiki/145/Configura%C3%A7%C3%A3o-do-reposit%C3%B3rio-privado'"
    exit 1
  fi
}

check_running () {
  IS_RUNNING=`docker-compose ps --services --filter "status=running"`
  if [[ "$IS_RUNNING" != "" ]]; then
      echo "The service is running!!!"
  fi
}

# Check if all required vars are defined
check_environment