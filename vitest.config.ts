import { defineConfig, configDefaults } from 'vitest/config'
import path from 'path'
import vue2 from '@vitejs/plugin-vue2'
import DefineOptions from 'unplugin-vue-define-options/vite'
export default defineConfig({
  plugins: [vue2(), DefineOptions()],
  extends: '@vue/tsconfig/tsconfig.web.json',
  test: {
    globals: true,
    environment: 'jsdom',
    environmentOptions: {
      NODE_ENV: 'test'
    },
    include: ['src/**/*.spec.ts'],
    exclude: [
      '**/@locaweb/design-system-icons/**',
      '**/node_modules/**',
      '**/dist/**',
      '**/cypress/**',
      '**/src/infrastructure/mocks/**',
      '**/src/infrastructure/http/**',
      '**/src/application/utils/TestUtils.ts',
      '**/src/application/config/**',
      '**/src/main.ts',
      ...configDefaults.exclude
    ],
    onConsoleLog: function (log) {
      if (log.includes('Download the Vue Devtools extension')) {
        return false
      }
    },
    coverage: {
      all: true,
      reporter: ['text', ['html'], 'lcov', ['json', { file: 'coverage.json' }]],
      extension: ['.js', '.ts', '.tsx', '.jsx', '.vue'],
      include: ['src/**/*.ts', 'src/**/*.vue'],
      exclude: [
        'src/application/config/**/*.ts',
        'src/application/utils/TestUtils.ts',
        'src/application/locales/**/*.ts',
        'src/application/router/**/*.ts',
        'src/application/**/router/*.ts',
        'src/*.ts',
        'src/infrastructure/**/*.ts',
        'src/**/types.ts',
        '**/**/*.d.ts',
        '**/**/*.spec.ts'
      ]
    }
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@task': path.resolve(__dirname, './src/application/view/task'),
      '@dashboard': path.resolve(__dirname, './src/application/view/dashboard'),
      '@tests': path.resolve(__dirname, './tests'),
      '@components': path.resolve(__dirname, './src/application/components'),
      '@images': path.resolve(__dirname, './src/application/assets/images')
    }
  }
})
