export interface IHttpError {
  status: number
  kind: string
  message: string
  cause?: unknown
}

export class HttpError extends Error implements IHttpError {
  status: number
  kind: string
  cause?: unknown
  constructor(httpErrorParams: IHttpError) {
    super(httpErrorParams.message)
    Object.setPrototypeOf(this, HttpError.prototype)
    this.status = httpErrorParams.status
    this.kind = httpErrorParams.kind
    this.cause = httpErrorParams.cause
  }
}
