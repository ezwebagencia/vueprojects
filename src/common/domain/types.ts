export type PaginationType = {
  page: number
  limit: number
  totalItens?: number
  totalPages?: number
}
