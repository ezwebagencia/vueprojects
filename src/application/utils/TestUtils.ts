import DesignSystem from '@locaweb/design-system-vue'
import Vue from 'vue'
import locales from '../locales'
import routes from '../router'
import TestUtils from '@locaweb/lw-test-utils'

Vue.use(DesignSystem, {
  locales,
  router: { routes }
})

export default new TestUtils(Vue, DesignSystem)
