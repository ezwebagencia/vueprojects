import User, { UserInfo } from '@/core/auth/domain/User'
import { defineStore } from 'pinia'

export const useAuthStore = defineStore('authStore', {
  state: (): { user: User } => {
    return {
      user: new User({
        email: '',
        username: '',
        fullName: ''
      })
    }
  },
  getters: {
    userInfo(): UserInfo {
      return this.user.userInfo
    }
  },
  actions: {
    login(user: User) {
      this.user = user
    }
  }
})
