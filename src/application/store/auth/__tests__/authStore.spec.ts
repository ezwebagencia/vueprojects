import { defineComponent } from 'vue'
import { useAuthStore } from '../index'
import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import User from '@/core/auth/domain/User'
import { Wrapper } from '@vue/test-utils'

describe('Test if authStore is working as expected', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: Wrapper<Vue> | any

  const user = new User({
    email: 'lorem@gmail.com',
    fullName: 'lorem ipsum',
    username: 'lomreipums'
  })
  beforeAll(() => {
    wrapper = TestUtils.mountTest(
      MountEnum.SHALLOW,
      defineComponent({
        setup() {
          const auth = useAuthStore()
          return { auth }
        },
        template: '<div></div>'
      })
    )
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should return a empty user info', () => {
    expect(wrapper.vm.auth.user).toEqual(
      new User({
        email: '',
        fullName: '',
        username: ''
      })
    )
  })

  it('should return user info after login', () => {
    wrapper.vm.auth.login(user)
    expect(wrapper.vm.auth.user).toEqual(user)
  })
})
