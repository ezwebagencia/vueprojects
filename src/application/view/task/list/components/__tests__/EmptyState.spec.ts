import { Wrapper } from '@vue/test-utils'
import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import EmptyState from '../EmptyState.vue'

type EmptyStateType = {
  _setupState: { state: { value: { illustration: string } } }
}

describe('Test if EmptyState component behave as expected', () => {
  let wrapper: Wrapper<Vue & EmptyStateType>
  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, EmptyState)
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should render a empty state for a empty task list', () => {
    expect(wrapper.findComponent({ name: 'LwEmptyState' }).props().title).toBe(
      'Oops, você ainda não tem nenhuma tarefa cadastrada.'
    )
    expect(wrapper.vm._setupState.state.value.illustration).toBe(
      '/src/application/assets/tasks/empty-list.svg'
    )
  })

  it('should render a empty state for error when try to request a list of tasks', async () => {
    await wrapper.setProps({
      error: true
    })
    expect(wrapper.findComponent({ name: 'LwEmptyState' }).props().title).toBe(
      'Oops, o serviço está indisponível no momento.'
    )
    expect(wrapper.vm._setupState.state.value.illustration).toBe(
      '/src/application/assets/results/server-error.svg'
    )
  })

  it('should render a empty state for a empty task list when has a filter', async () => {
    await wrapper.setProps({
      filter: 'lorem',
      error: false
    })
    expect(wrapper.findComponent({ name: 'LwEmptyState' }).props().title).toBe(
      'Oops, nenhuma tarefa encontrada com o termo: lorem'
    )
    expect(wrapper.vm._setupState.state.value.illustration).toBe(
      '/src/application/assets/tasks/empty-filter.svg'
    )
  })
})
