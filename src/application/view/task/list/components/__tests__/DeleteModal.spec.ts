import { Wrapper } from '@vue/test-utils'
import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import DeleteModalVue from '../DeleteModal.vue'
import Vue from 'vue'

type DeleteModalType = {
  _setupState: { confirmTitle: { value: string } }
}

describe('Test if DeleteModal component behave as expected', () => {
  let wrapper: Wrapper<Vue & DeleteModalType>
  const task = {
    id: 1,
    title: 'Lorem',
    description: 'Lorem ipsum dolor sit amet',
    done: false
  }
  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.MOUNT, DeleteModalVue, {
      propsData: {
        task
      },
      stubs: ['LwButton', 'LwTextField']
    })
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should render a modal with task delete confirmation', () => {
    expect(wrapper.html()).toMatchSnapshot()
    expect(wrapper.findComponent({ name: 'LwModal' }).exists()).toBeTruthy()
    expect(wrapper.findAllComponents({ name: 'LwButton' }).length).toBe(2)
    expect(wrapper.findComponent({ name: 'LwTextField' }).exists).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwHTMLRender' }).text()).toBe(
      'Você tem certeza que deseja excluir a tarefa: Lorem? Informe o título da tarefa no campo a baixo para confirmar a exclusão.'
    )
  })

  it('should have a disabled delete button when confirm title is not equal task title', async () => {
    expect(wrapper.findAllComponents({ name: 'LwButton' }).at(1).attributes().disabled).toBeTruthy()
    wrapper.vm._setupState.confirmTitle.value = task.title
    await wrapper.vm.$nextTick()
    expect(wrapper.findAllComponents({ name: 'LwButton' }).at(1).attributes().disabled).toBeFalsy()
  })

  it('should emit delete task on click delete button', async () => {
    await wrapper.find('form').trigger('submit')
    expect(wrapper.emitted().submit).toBeTruthy()
  })
})
