import { Wrapper } from '@vue/test-utils'
import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import FormModal from '../FormModal.vue'
import Vue from 'vue'

type FormModalType = { _setupState: { v$: { value: { $validate: () => void } } } }
describe('Test if FormModal component behave as expected', () => {
  let wrapper: Wrapper<Vue & FormModalType>
  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.MOUNT, FormModal, {
      stubs: ['LwButton', 'LwTextField', 'LwTextAreaField', 'LwSelectionField', 'VDialog']
    })
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should render a form with title and description fields', () => {
    expect(wrapper.html()).toMatchSnapshot()
    expect(wrapper.findComponent({ name: 'LwTextField' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwTextField' }).attributes().label).toBe('Título')
    expect(wrapper.findComponent({ name: 'LwTextAreaField' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwTextAreaField' }).attributes().label).toBe('Descrição')
  })

  it('should not submit form when is invalid', async () => {
    await wrapper.find('form').trigger('submit')
    expect(wrapper.emitted().submit).toBeFalsy()
    wrapper.findComponent({ name: 'LwTextField' }).vm.$emit('input', 'lorem')
    wrapper.findComponent({ name: 'LwTextAreaField' }).vm.$emit('input', 'lorem ipsum dollor')
    await wrapper.find('form').trigger('submit')
    await wrapper.vm._setupState.v$.value.$validate()
    expect(wrapper.emitted().submit).toBeTruthy()
  })

  it('should render create title with task hasnt id', () => {
    expect(wrapper.findComponent({ name: 'LwModal' }).props().title).toBe('Adicionar tarefa')
  })

  it('should render edit title and checkbox to changhe status when task has id', async () => {
    await wrapper.setProps({
      task: {
        id: 1
      }
    })
    expect(wrapper.html()).toMatchSnapshot()
    expect(wrapper.findComponent({ name: 'LwModal' }).props().title).toBe('Editar tarefa')
  })
})
