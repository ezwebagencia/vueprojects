import { Wrapper } from '@vue/test-utils'
import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import TaskComponentVue from '../TaskComponent.vue'

describe('Test if Task component behave as expected', () => {
  let wrapper: Wrapper<Vue>

  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, TaskComponentVue, {
      propsData: {
        task: {
          id: 1,
          title: 'Lorem',
          description: 'Lorem ipsum dolor sit amet',
          done: false
        }
      }
    })
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should render an InfoText state with task data and button to create and edit', () => {
    expect(wrapper.findComponent({ name: 'LwInfoText' }).exists()).toBeTruthy()
    expect(wrapper.findAllComponents({ name: 'LwButton' }).length).toBe(2)
    expect(wrapper.findComponent({ name: 'LwInfoText' }).props().label).toBe('Lorem')
    expect(wrapper.findComponent({ name: 'LwInfoText' }).props().text).toBe(
      'Lorem ipsum dolor sit amet'
    )
    wrapper
      .findAllComponents({ name: 'LwButton' })
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .wrappers.forEach((button: Wrapper<any>, index: number) => {
        expect(button.props().icon).toBe(
          index === 0 ? '$objects-pencil-filled' : '$objects-trash-filled'
        )
        expect(button.attributes().title).toBe(
          index === 0 ? 'Editar tarefa Lorem' : 'Excluir tarefa Lorem'
        )
      })
  })

  it('should render disabled delete button when task is done', async () => {
    expect(wrapper.findAllComponents({ name: 'LwButton' }).at(1).props().disabled).toBeFalsy()
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, TaskComponentVue, {
      propsData: { task: { ...wrapper.props().task, done: true } }
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.findAllComponents({ name: 'LwButton' }).at(1).props().disabled).toBeTruthy()
  })

  it('should emit edit task on click edit button', () => {
    wrapper.findAllComponents({ name: 'LwButton' }).at(0).vm.$emit('click')
    expect(wrapper.emitted().select).toBeTruthy()
    expect(wrapper.emitted().select?.pop()?.[0]).toEqual({
      type: 'edit',
      task: wrapper.props().task
    })
  })

  it('should emit delete task on click delete button', () => {
    wrapper.findAllComponents({ name: 'LwButton' }).at(1).vm.$emit('click')
    expect(wrapper.emitted().select).toBeTruthy()
    expect(wrapper.emitted().select?.pop()?.[0]).toEqual({
      type: 'delete',
      task: wrapper.props().task
    })
  })
})
