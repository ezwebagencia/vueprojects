import { defineComponent } from 'vue'
import { useTaskValidation } from '../taskValidation'

import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import Task from '@/core/task/domain/Task'
import { Wrapper } from '@vue/test-utils'

type TaskValidation = {
  title: {
    $invalid: boolean
    $errors: { $validator: string }[]
  }
  description: {
    $invalid: boolean
    $errors: { $validator: string }[]
  }
}

describe('Test if taskValidation function behave as expected', () => {
  let wrapper: Wrapper<Vue & { task: { title: string; description: string }; v$: TaskValidation }>

  beforeAll(() => {
    wrapper = TestUtils.mountTest(
      MountEnum.SHALLOW,
      defineComponent({
        setup() {
          return useTaskValidation(new Task(), {})
        },
        template: '<div></div>'
      })
    )
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should return a ref state and a vuelidate instancie', () => {
    expect(wrapper.vm.task).toBeDefined()
    expect(wrapper.vm.v$).toBeDefined()
  })

  it('should have title as invalid when leaves field empty', async () => {
    wrapper.vm.task.title = 'lorem'
    wrapper.vm.task.title = ''
    expect(wrapper.vm.v$.title.$invalid).toBeTruthy()
    expect(wrapper.vm.v$.title.$errors.pop()?.$validator).toBe('required')
  })

  it('should have required validation of title as true', async () => {
    expect(wrapper.vm.v$.title.$invalid).toBeTruthy()
    wrapper.vm.task.title = 'lorem'
    expect(wrapper.vm.v$.title.$invalid).toBeFalsy()
  })

  it('should have title as invalid when user sets a value with size less then 3', async () => {
    wrapper.vm.task.title = 'lo'
    expect(wrapper.vm.v$.title.$invalid).toBeTruthy()
    expect(wrapper.vm.v$.title.$errors.pop()?.$validator).toBe('minLength')
    wrapper.vm.task.title = 'lor'
    expect(wrapper.vm.v$.title.$invalid).toBeFalsy()
  })

  it('shoud have title as valid when user sets a value greater than 2', () => {
    wrapper.vm.task.title = 'lo'
    expect(wrapper.vm.v$.title.$invalid).toBeTruthy()
    expect(wrapper.vm.v$.title.$errors.pop()?.$validator).toBe('minLength')
    wrapper.vm.task.title = 'lor'
    expect(wrapper.vm.v$.title.$invalid).toBeFalsy()
  })

  it('should have description as invalid when leaves field empty', async () => {
    wrapper.vm.task.description = 'lorem'
    wrapper.vm.task.description = ''
    expect(wrapper.vm.v$.description.$invalid).toBeTruthy()
    expect(wrapper.vm.v$.description.$errors.pop()?.$validator).toBe('required')
  })

  it('should have required validation of description as true', async () => {
    expect(wrapper.vm.v$.description.$invalid).toBeTruthy()
    wrapper.vm.task.description = 'lorem ipsum dolor sit amet'
    expect(wrapper.vm.v$.description.$invalid).toBeFalsy()
  })

  it('should have description as invalid when user sets a value with size less then 10', async () => {
    wrapper.vm.task.description = 'lorem i'
    expect(wrapper.vm.v$.description.$invalid).toBeTruthy()
    expect(wrapper.vm.v$.description.$errors.pop()?.$validator).toBe('minLength')
  })

  it('shoud have description as valid when user sets a value greater than 9', () => {
    wrapper.vm.task.description = 'lorem i'
    expect(wrapper.vm.v$.description.$invalid).toBeTruthy()
    wrapper.vm.task.description = 'lorem ipsum dolor sit amet'
    expect(wrapper.vm.v$.description.$invalid).toBeFalsy()
  })
})
