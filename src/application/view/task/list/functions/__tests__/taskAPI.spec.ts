import { UseTaskAPIInterface } from './../taskAPI'
import { defineComponent } from 'vue'
import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import useTaskAPI from '../taskAPI'
import TasksPaginated from '../../__tests__/mocks/TasksPaginated'
import Task from '@/core/task/domain/Task'
import { httpClient } from '@/infrastructure/http/HttpClient'
import { Wrapper } from '@vue/test-utils'
import flushPromises from 'flush-promises'

describe('Test if taskAPI function behave as expected', () => {
  let wrapper: Wrapper<Vue & UseTaskAPIInterface>
  const findAllRequest = vi.fn().mockReturnValue(TasksPaginated)
  httpClient.get = findAllRequest
  const saveRequest = vi.fn().mockReturnValue({})
  httpClient.post = saveRequest
  const notifyFunction = vi.fn()
  const $notify = {
    show: notifyFunction
  }
  const component = defineComponent({
    setup() {
      return useTaskAPI()
    },
    template: '<div></div>'
  })

  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, component, {
      mocks: {
        $notify
      }
    })
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should call findAll request on component mount', () => {
    expect(wrapper.vm.loading).toBeFalsy()
    expect(findAllRequest).toHaveBeenCalled()
    expect(wrapper.vm.loading).toBeFalsy()
    expect(findAllRequest.mock.calls[0][1]).toEqual({ params: { page: 1, limit: 10 } })
  })

  it('should call api when user change page', () => {
    wrapper.vm.handlePagination(2)
    expect(findAllRequest.mock.calls.pop()[1]).toEqual({ params: { page: 2, limit: 10 } })
  })

  it('should call api when user change filter', () => {
    wrapper.vm.handleFilter('lorem')
    expect(findAllRequest.mock.calls.pop()[1]).toEqual({
      params: { filter: 'lorem', page: 1, limit: 10 }
    })
  })

  it('should call api for create a task when user submit form', async () => {
    const closeModal = vi.fn()
    await wrapper.vm.handleSave(new Task('lorem', 'ipsum'), closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect(saveRequest.mock.calls.pop()[1]).toEqual(new Task('lorem', 'ipsum'))
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message: 'Tarefa foi criada com sucesso.',
      state: 'success'
    })
  })

  it('should return a bad request notify when user submit a invalid form', async () => {
    const closeModal = vi.fn()
    const rejectRequest = vi
      .fn()
      .mockRejectedValue({ response: { status: 400, data: { errors: { titile: 'blank' } } } })
    httpClient.post = rejectRequest
    await wrapper.vm.handleSave(new Task('lorem', 'ipsum'), closeModal)
    expect(closeModal).not.toHaveBeenCalled()
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message: 'Erro ao tentar criar a tarefa; valide o formulário e tente novamente.',
      state: 'error'
    })
  })

  it('should return a server error request notify when server cannot create a task', async () => {
    const closeModal = vi.fn()
    const rejectRequest = vi.fn().mockRejectedValue({ response: { status: 500 } })
    httpClient.post = rejectRequest
    await wrapper.vm.handleSave(new Task('lorem', 'ipsum'), closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message:
        'Houve um problema ao tentar criar a tarefa, estamos tentando resolver; tente novamente em breve.',
      state: 'error'
    })
  })

  it('should call api for edit a task when user submit form', async () => {
    const task = new Task('lorem', 'ipsum', false, 1)
    const closeModal = vi.fn()
    const editRequest = vi.fn().mockReturnValue({ status: 200 })
    httpClient.put = editRequest
    await wrapper.vm.handleSave(task, closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect(editRequest.mock.calls.pop()[1]).toEqual(task)
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message: 'Tarefa foi alterada com sucesso.',
      state: 'success'
    })
  })

  it('should return a bad request notify when user submit a invalid form for edit a task', async () => {
    const task = new Task('lorem', 'ipsum', false, 1)
    const closeModal = vi.fn()
    const rejectRequest = vi
      .fn()
      .mockRejectedValue({ response: { status: 400, data: { errors: { titile: 'blank' } } } })
    httpClient.put = rejectRequest
    await wrapper.vm.handleSave(task, closeModal)
    expect(closeModal).not.toHaveBeenCalled()
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message: 'Erro ao tentar alterar a tarefa; valide o formulário e tente novamente.',
      state: 'error'
    })
  })

  it('should return a conflict request notify when user submit a invalid form for edit a task', async () => {
    const task = new Task('lorem', 'ipsum', false, 1)
    const closeModal = vi.fn()
    const rejectRequest = vi.fn().mockRejectedValue({ response: { status: 409 } })
    httpClient.put = rejectRequest
    await wrapper.vm.handleSave(task, closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message:
        'Erro ao tentar alterar a tarefa, o status da tarefa é conflitante; tente novamente em breve.',
      state: 'error'
    })
  })

  it('should return a server error request notify when server cannot edit a task', async () => {
    const task = new Task('lorem', 'ipsum', false, 1)
    const closeModal = vi.fn()
    const rejectRequest = vi.fn().mockRejectedValue({ response: { status: 500 } })
    httpClient.put = rejectRequest
    await wrapper.vm.handleSave(task, closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message:
        'Houve um problema ao tentar alterar a tarefa, estamos tentando resolver; tente novamente em breve.',
      state: 'error'
    })
  })

  it('should call api for delete a task when user confirms deletion', async () => {
    const task = new Task('lorem', 'ipsum', false, 1)
    const closeModal = vi.fn()
    const deleteRequest = vi.fn().mockReturnValue({ status: 200 })
    httpClient.delete = deleteRequest
    await wrapper.vm.handleDelete(task, closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect(deleteRequest.mock.calls.pop()[0]).toBe(`/tasks/${task.id}`)
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message: 'Tarefa excluída com sucesso.',
      state: 'success'
    })
  })

  it('should return a conflict request notify when user try delete task with done status', async () => {
    const task = new Task('lorem', 'ipsum', false, 1)
    const closeModal = vi.fn()
    const rejectRequest = vi.fn().mockRejectedValue({ response: { status: 409 } })
    httpClient.delete = rejectRequest
    await wrapper.vm.handleDelete(task, closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect(rejectRequest.mock.calls.pop()[0]).toBe(`/tasks/${task.id}`)
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message:
        'Houve um erro ao tentar excluir a tarefa; aparentemente ela se encontra com status de concluída.',
      state: 'error'
    })
  })

  it('should return a server error request notify when server cannot delete a task', async () => {
    const task = new Task('lorem', 'ipsum', false, 1)
    const closeModal = vi.fn()
    const rejectRequest = vi.fn().mockRejectedValue({ response: { status: 500 } })
    httpClient.delete = rejectRequest
    await wrapper.vm.handleDelete(task, closeModal)
    expect(closeModal).toHaveBeenCalled()
    expect(rejectRequest.mock.calls.pop()[0]).toBe(`/tasks/${task.id}`)
    expect($notify.show.mock.calls.pop()[0]).toEqual({
      message:
        'Houve um problema ao tentar excluir a tarefa, estamos tentando resolver; tente novamente em breve.',
      state: 'error'
    })
  })

  it('should return error state when server cannot list tasks', async () => {
    httpClient.get = vi.fn().mockRejectedValue({ response: { status: 500 } })
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, component, {
      mocks: {
        $notify
      }
    })
    await flushPromises()
    expect(wrapper.vm.error).toBeTruthy()
  })
})
