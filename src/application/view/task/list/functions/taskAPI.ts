import { ServerErrors } from '@vuelidate/core'
import Task from '@/core/task/domain/Task'
import { TaskError, TaskErrorKind } from '@/core/task/infra/error/TaskError'
import ListUseCase from '@/core/task/usecases/ListUseCase'
import SaveUseCase from '@/core/task/usecases/SaveUseCase'
import { NotificationState, useLocale, useNotification } from '@locaweb/design-system-vue'
import { onMounted, ref } from 'vue'
import DeleteUseCase from '@/core/task/usecases/DeleteUseCase'
import { FindAllParams } from '@/core/task/domain/FindAllParams'

const TaskErrors: { [key: string]: string } = {
  BAD_REQUEST: 'badRequest',
  SERVER_ERROR: 'serverError',
  NOT_FOUND: 'notFound',
  CONFLICT: 'conflict'
}

export interface UseTaskAPIInterface {
  loading: boolean
  taskList: Task[]
  listParams: FindAllParams
  handleFilter(filter: string): void
  handlePagination(page: number): void
  error: boolean
  handleSave(task: Task, hideModal?: () => void): void
  serverErrors: ServerErrors
  handleDelete(task: Task, hideModal?: () => void): void
}

export default function useTaskAPI() {
  const taskList = ref<Task[]>([])
  const loading = ref(true)
  const error = ref(false)
  const $notify = useNotification()
  const serverErrors = ref({} as ServerErrors)
  const listParams = ref<FindAllParams>(new FindAllParams())
  const $t = useLocale('task.list.alerts')

  function handleFilter(filter: string) {
    listParams.value = new FindAllParams(filter, {
      page: 1,
      limit: 10
    })
    execute()
  }

  function handlePagination(page: number) {
    listParams.value.pagination.page = page
    execute()
  }

  async function execute(): Promise<void> {
    loading.value = true
    try {
      const { tasks, pagination } = await ListUseCase.listAll(listParams.value)
      taskList.value = tasks
      listParams.value.pagination = pagination
    } catch (e) {
      error.value = true
    } finally {
      loading.value = false
    }
  }

  async function handleSave(task: Task, hideModal: () => void) {
    const isEdit = !!task.id
    try {
      await SaveUseCase.save(task)
      hideModal?.()
      handlePagination(1)
      $notify.show({
        message: $t[isEdit ? 'edit' : 'create'].success,
        state: NotificationState.success
      })
    } catch (e) {
      if (e instanceof TaskError) {
        if (e.status === TaskErrorKind.BAD_REQUEST && e.errors) {
          serverErrors.value = e.errors
        } else {
          hideModal?.()
        }
        $notify.show({
          message: $t[isEdit ? 'edit' : 'create'].error[TaskErrors[e.kind]],
          state: NotificationState.error
        })
      }
    }
  }

  async function handleDelete(task: Task, hideModal: () => void) {
    try {
      await DeleteUseCase.delete(task)
      hideModal?.()
      handlePagination(1)
      $notify.show({
        message: $t.delete.success,
        state: NotificationState.success
      })
    } catch (e) {
      if (e instanceof TaskError) {
        hideModal?.()
        $notify.show({
          message: $t.delete.error[TaskErrors[e.kind]],
          state: NotificationState.error
        })
      }
    }
  }

  onMounted(async () => {
    execute()
  })

  return {
    loading,
    taskList,
    listParams,
    handleFilter,
    handlePagination,
    error,
    handleSave,
    serverErrors,
    handleDelete
  }
}
