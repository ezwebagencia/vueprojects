import Task from '@/core/task/domain/Task'
import { mountValidator } from '@locaweb/design-system-vue'
import { required, minLength } from '@vuelidate/validators'
import { useVuelidate, ServerErrors } from '@vuelidate/core'
import { reactive } from 'vue'

export function useTaskValidation(initialState: Task, $externalResults: ServerErrors) {
  const task: Task = reactive(initialState)

  const rules = {
    title: {
      required: mountValidator(required),
      minLength: mountValidator(minLength(3))
    },
    description: {
      required: mountValidator(required),
      minLength: mountValidator(minLength(10))
    }
  }

  const v$ = useVuelidate(rules, task, { $autoDirty: true, $externalResults })

  return { task, v$ }
}
