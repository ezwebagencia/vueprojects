import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import Vue from 'vue'
import TaskList from '@/application/view/task/list/TaskList.vue'
import { Wrapper } from '@vue/test-utils'
import ListUseCase from '@/core/task/usecases/ListUseCase'
import SaveUseCase from '@/core/task/usecases/SaveUseCase'
import DeleteUseCase from '@/core/task/usecases/DeleteUseCase'
import TasksPaginated from './mocks/TasksPaginated'
import { FindAllParams } from '@/core/task/domain/FindAllParams'

describe('Test if DashboardView component behave as expected', () => {
  let wrapper: Wrapper<Vue>
  const findAllRequest = vi.fn().mockReturnValue(TasksPaginated)
  ListUseCase.listAll = findAllRequest
  const saveRequest = vi.fn().mockReturnValue({})
  SaveUseCase.save = saveRequest
  const deleteRequest = vi.fn().mockReturnValue({})
  DeleteUseCase.delete = deleteRequest

  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, TaskList)
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should call findAll on mounted', async () => {
    expect(findAllRequest).toHaveBeenCalled()
  })

  it('should render a list of tasks and pagination component', async () => {
    expect(wrapper.findComponent({ name: 'LwList' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwPagination' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwPagination' }).props().length).toBe(
      TasksPaginated.pagination.totalPages
    )
    expect(wrapper.find('[data-test="task-filter"]').exists()).toBeTruthy()
  })

  it('should render a emptyState when list of tasks is empty', async () => {
    await wrapper.setData({ taskList: [] })
    expect(wrapper.find('[data-test="empty-state"]').exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwPagination' }).exists()).toBeFalsy()
  })

  it('should render emptyState with error when server fails', async () => {
    await wrapper.setData({ error: true })
    expect(wrapper.find('[data-test="empty-state"]').exists()).toBeTruthy()
    expect(wrapper.find('[data-test="empty-state"]').attributes().error).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwPagination' }).exists()).toBeFalsy()
    expect(wrapper.find('[data-test="task-filter"]').exists()).toBeFalsy()
  })

  it('should call api when user change page', async () => {
    await wrapper.setData({ error: false, taskList: TasksPaginated.tasks })
    wrapper.findComponent('[data-test="task-pagination"]').vm.$emit('input', 2)
    expect(findAllRequest).toBeCalledTimes(2)
    expect(findAllRequest.mock.calls[1][0]).toEqual(
      new FindAllParams('', {
        page: 2,
        limit: 10,
        totalPages: 5
      })
    )
  })

  it('should call api when user sets a filter', async () => {
    wrapper.findComponent('[data-test="task-filter"]').vm.$emit('input', 'lorem')
    expect(findAllRequest).toBeCalledTimes(3)
    expect(findAllRequest.mock.calls[2][0]).toEqual(new FindAllParams('lorem'))
  })

  it('should display task form modal when user clicks on create button', async () => {
    wrapper.findComponent({ name: 'LwHeading' }).props().actionEvents.click({})
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent('[data-test="task-modal"]').exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'FormModal' }).exists()).toBeTruthy()
  })

  it('should call api to create a new task when user submits form', async () => {
    wrapper.findComponent({ name: 'LwHeading' }).props().actionEvents.click({})
    await wrapper.vm.$nextTick()
    wrapper.findComponent({ name: 'FormModal' }).vm.$emit('submit', TasksPaginated.tasks[0])
    expect(saveRequest.mock.calls.pop()[0]).toEqual(TasksPaginated.tasks[0])
  })

  it('should display task form modal when user clicks on edit button', async () => {
    await wrapper.setData({
      modalProps: {
        display: false
      }
    })
    await wrapper.vm.$nextTick()
    wrapper
      .findAllComponents('[data-test="task-item"]')
      .at(0)
      .vm.$emit('select', TasksPaginated.tasks[0])
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent('[data-test="task-modal"]').exists()).toBeTruthy()
  })

  it('should call api to edit a new task when user submits form', async () => {
    await wrapper.setData({
      modalProps: {
        display: false
      }
    })
    await wrapper.vm.$nextTick()
    wrapper
      .findAllComponents('[data-test="task-item"]')
      .at(0)
      .vm.$emit('select', { task: TasksPaginated.tasks[0] })
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent('[data-test="task-modal"]').exists()).toBeTruthy()
    wrapper.findComponent({ name: 'FormModal' }).vm.$emit('submit', TasksPaginated.tasks[0])
    expect(saveRequest.mock.calls.pop()[0]).toEqual(TasksPaginated.tasks[0])
  })

  it('should display task delete modal when user clicks on delete button', async () => {
    wrapper
      .findAllComponents('[data-test="task-item"]')
      .at(0)
      .vm.$emit('select', { task: TasksPaginated.tasks[0], type: 'delete' })
    await wrapper.vm.$nextTick()
    expect(wrapper.findComponent('[data-test="task-modal"]').exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'DeleteModal' }).exists()).toBeTruthy()
  })
})
