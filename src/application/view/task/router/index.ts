import { RouteConfig } from 'vue-router'

const TaskList = () => import('@task/list/TaskList.vue')

const PREFIX = '/tarefas'

export const TaskRoutesPath: { [key: string]: RouteConfig } = Object.freeze({
  LISTAR: {
    name: 'taskList',
    title: 'task.list.title',
    path: PREFIX,
    component: TaskList
  }
})

export default Object.values(TaskRoutesPath)
