import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import DashboardView from '@/application/view/dashboard/DashboardView.vue'
import { Wrapper } from '@vue/test-utils'
import Vue from 'vue'
import DashboardUseCase from '@/core/dashboard/usecases/DashboardUseCase'
import report from '../components/__tests__/mocks/report'

describe('Test if DashboardView component behave as expected', () => {
  let wrapper: Wrapper<Vue>
  const request = vi.fn().mockResolvedValue(report)
  DashboardUseCase.getReport = request
  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, DashboardView)
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should render InfoCards with summary data, a list of last tasks and a pricing banner', async () => {
    expect(wrapper.findComponent({ name: 'InfoCards' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LastTasks' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwPricing' }).exists()).toBeTruthy()
    expect(request).toHaveBeenCalled()
    expect(wrapper.html()).toMatchSnapshot()
  })
})
