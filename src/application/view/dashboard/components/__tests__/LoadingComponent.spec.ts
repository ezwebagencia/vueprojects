import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import LoadingComponent from '@/application/view/dashboard/components/LoadingComponent.vue'
import { Wrapper } from '@vue/test-utils'
import Vue from 'vue'

describe('Test if Loading component behave as expected', () => {
  let wrapper: Wrapper<Vue>
  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, LoadingComponent)
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should render three cards Skeletons', () => {
    expect(wrapper.findAll('[data-test="card-load"]').length).toBe(3)
    expect(wrapper.findAll('[data-test="card-load"]').at(0).attributes().type).toBe(
      'table-heading, actions'
    )
  })

  it('should render a skeleton as list', () => {
    expect(wrapper.findAll('[data-test="list-load"]').length).toBe(1)
    expect(wrapper.find('[data-test="list-load"]').attributes().type).toBe(
      'table-heading,list-item-two-line,divider,list-item-two-line,divider,list-item-two-line,divider,list-item-two-line,divider,list-item-two-line, actions'
    )
  })
})
