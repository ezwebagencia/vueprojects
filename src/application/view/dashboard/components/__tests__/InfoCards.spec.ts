import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import InfoCards from '@/application/view/dashboard/components/InfoCards.vue'
import { Wrapper } from '@vue/test-utils'
import report from './mocks/report'

describe('Test if TheApp component behave as expected', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: Wrapper<Vue> | any

  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, InfoCards, {
      propsData: {
        summary: report.summary
      }
    })
  })
  afterAll(() => {
    wrapper.destroy()
  })
  it('should render 3 cards components for display summary data', () => {
    expect(wrapper.findAllComponents({ name: 'LwCard' }).length).toBe(3)
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('should render first card with total of with done status', () => {
    expect(wrapper.findAllComponents({ name: 'LwCard' }).at(0).vm.title).toBe('Tarefas Concluídas')
    expect(
      wrapper.findAllComponents({ name: 'LwCard' }).at(0).find('[data-test="card-content"]').text()
    ).toBe('5Contador de tarefas')
  })

  it('should render second card with total of task with pending status', () => {
    expect(wrapper.findAllComponents({ name: 'LwCard' }).at(1).vm.title).toBe('Tarefas Pendentes')
    expect(
      wrapper.findAllComponents({ name: 'LwCard' }).at(1).find('[data-test="card-content"]').text()
    ).toBe('3Contador de tarefas')
  })

  it('should render third cardif total tasks', () => {
    expect(wrapper.findAllComponents({ name: 'LwCard' }).at(2).vm.title).toBe('Total de Tarefas')
    expect(
      wrapper.findAllComponents({ name: 'LwCard' }).at(2).find('[data-test="card-content"]').text()
    ).toBe('8Contador de tarefas')
  })
})
