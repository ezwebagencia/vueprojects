import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import LastTasks from '@/application/view/dashboard/components/LastTasks.vue'
import { Wrapper } from '@vue/test-utils'
import Vue from 'vue'
import report from './mocks/report'

describe('Test if Loading component behave as expected', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: Wrapper<Vue> | any

  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, LastTasks, {
      propsData: {
        report
      }
    })
  })

  afterAll(() => {
    wrapper.destroy()
  })

  it('should render a list component with 1 item', () => {
    expect(wrapper.findComponent({ name: 'LwCard' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwList' }).exists()).toBeTruthy()
    expect(wrapper.findComponent({ name: 'LwList' }).vm.items).toEqual(report.lastTasks)
  })
})
