import { RouteConfig } from 'vue-router'

const DashboardView = () => import('@dashboard/DashboardView.vue')

const PREFIX = '/'

export const DashboardRoutesPath: { [key: string]: RouteConfig } = Object.freeze({
  INDEX: {
    name: 'dashboard',
    path: PREFIX,
    component: DashboardView
  }
})

export default Object.values(DashboardRoutesPath)
