import { defineComponent } from 'vue'
import { useDashboardAPI } from '../dashboardAPI'
import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import DashboardUseCase from '@/core/dashboard/usecases/DashboardUseCase'
import report from '../../components/__tests__/mocks/report'
import { Wrapper } from '@vue/test-utils'

describe('Test if dashboardAPI function behave as expected', () => {
  const reportRequest = vi.fn().mockResolvedValue(report)
  DashboardUseCase.getReport = reportRequest

  it('should call api when mounted and return report', async () => {
    const wrapper: Wrapper<Vue & { loading: boolean; report: unknown }> = TestUtils.mountTest(
      MountEnum.SHALLOW,
      defineComponent({
        setup() {
          return { ...useDashboardAPI() }
        },
        template: '<div></div>'
      })
    )
    await wrapper.vm.$nextTick()
    expect(reportRequest).toHaveBeenCalled()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.loading).toBeFalsy()
    expect(wrapper.vm.report).toEqual(report)
    wrapper.destroy()
  })
})
