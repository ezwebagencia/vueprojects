import Report from '@/core/dashboard/domain/Report'
import DashboardUseCase from '@/core/dashboard/usecases/DashboardUseCase'
import { Ref, onMounted, ref } from 'vue'

export function useDashboardAPI(): { loading: Ref<boolean>; report: Ref<Report | null> } {
  const loading: Ref<boolean> = ref(true)
  const report: Ref<Report | null> = ref(null)

  onMounted(async () => {
    report.value = await DashboardUseCase.getReport()
    loading.value = false
  })

  return { loading, report }
}
