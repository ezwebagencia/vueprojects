import _Vue from 'vue'
import IconsPlugin from '@locaweb/design-system-icons'
import DesignSystem from '@locaweb/design-system-vue'
import locales from '../locales'
import routes from '../router'
import { PiniaVuePlugin } from 'pinia'
import { StartOptions } from 'msw'

declare type Color = {
  base: string
  lighten2?: string
  lighten1?: string
  darken1?: string
  darken2?: string
}
declare type Theme = {
  light?: {
    [key: string]: Color | string
  }
  dark?: {
    [key: string]: Color | string
  }
  font?: {
    default: string
    mono: string
  }
}

export default {
  install(vue: typeof _Vue) {
    const theme: Theme = {
      light: {
        primary: {
          base: '#e4d5f4',
          lighten2: '#FFC8DB',
          lighten1: '#FA749F',
          darken1: '#BC1A44',
          darken2: '#8F1434'
        },
        secondary: '#2f0f00',
        error: '#2e2000',
        success: '#272829',
        info: '#272829',
        warning: '#272829',
        highlight: '#272829'
      },
      dark: {
        primary: '#272829',
        secondary: '#272829',
        error: '#272829',
        success: '#272829',
        info: '#272829',
        warning: '#272829',
        highlight: '#272829'
      },
      font: {
        default: 'Archivo',
        mono: 'Work Sans'
      }
    }
    console.log(import.meta.env)
    vue.use(PiniaVuePlugin)
    vue.use(DesignSystem, {
      theme,
      locales,
      env: import.meta.env.MODE,
      router: { routes },
      sentry: {
        dsn: 'https://2a6328145c2a486db82dcd078122ff98@sentry.locaweb.com.br/638',
        env: import.meta.env.MODE
      }
    })
    vue.use(IconsPlugin)

    if (import.meta.env.MODE === 'development') {
      document.cookie = 'auth=test; expires=Sun, 1 Dez 2083 00:00:00 UTC; path=/'
      const { worker } = import.meta.glob('../../infrastructure/mocks/browser.ts', { eager: true })[
        '../../infrastructure/mocks/browser.ts'
      ] as { worker: { start: (options: StartOptions) => void } }
      worker.start({
        quiet: true,
        onUnhandledRequest: 'bypass'
      })
    }
  }
}
