export default {
  title: 'Olá, {name}',
  description: 'Acompanhe os seus objetivos e suas tarefas realizadas',
  summary: {
    title: {
      completed: 'Tarefas Concluídas',
      pending: 'Tarefas Pendentes',
      total: 'Total de Tarefas'
    },
    legend: 'Contador de tarefas'
  },
  pricing: {
    title: 'Plano plus',
    description:
      'Adquira o plano plus e tenha acesso a mais funcionalidades para tracking de suas tarefas e objetivos.',
    periodicity: 'mês',
    action: 'Assine agora'
  },
  lastTasks: {
    title: 'Últimas Tarefas Criadas',
    date: 'Atualizado em {date}',
    action: 'Visualizar'
  }
}
