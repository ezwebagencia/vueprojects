export default {
  createTitle: 'Adicionar tarefa',
  editTitle: 'Editar tarefa',
  fields: {
    title: 'Título',
    description: 'Descrição',
    status: 'Concluído'
  },
  actions: {
    save: 'Adicionar',
    edit: 'Alterar',
    cancel: 'Cancelar'
  }
}
