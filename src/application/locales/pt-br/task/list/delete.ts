export default {
  title: 'Excluir tarefa',
  description:
    'Você tem certeza que deseja excluir a tarefa: <b>{title}</b>? Informe o título da tarefa no campo a baixo para confirmar a exclusão.',
  confirmTitle: 'Título da tarefa',
  actions: {
    cancel: 'Cancelar',
    confirm: 'Excluir'
  }
}
