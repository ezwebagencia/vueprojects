export default {
  create: {
    success: 'Tarefa foi criada com sucesso.',
    error: {
      badRequest: 'Erro ao tentar criar a tarefa; valide o formulário e tente novamente.',
      serverError:
        'Houve um problema ao tentar criar a tarefa, estamos tentando resolver; tente novamente em breve.'
    }
  },
  edit: {
    success: 'Tarefa foi alterada com sucesso.',
    error: {
      badRequest: 'Erro ao tentar alterar a tarefa; valide o formulário e tente novamente.',
      serverError:
        'Houve um problema ao tentar alterar a tarefa, estamos tentando resolver; tente novamente em breve.',
      conflict:
        'Erro ao tentar alterar a tarefa, o status da tarefa é conflitante; tente novamente em breve.',
      notFound:
        'Erro ao tentar alterar a tarefa, não foi possível encontrá-la na base. Atualize a página e tente novamente.'
    }
  },
  delete: {
    success: 'Tarefa excluída com sucesso.',
    error: {
      serverError:
        'Houve um problema ao tentar excluir a tarefa, estamos tentando resolver; tente novamente em breve.',
      conflict:
        'Houve um erro ao tentar excluir a tarefa; aparentemente ela se encontra com status de concluída.',
      notFound:
        'Erro ao tentar excluir a tarefa, não foi possível encontrá-la na base. Atualize a página e tente novamente.'
    }
  }
}
