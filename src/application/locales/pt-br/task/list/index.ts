import form from './form'
import alerts from './alerts'
import deleteModal from './delete'
export default {
  title: 'Tarefas',
  actionTitle: 'Adicionar Tarefa',
  filter: {
    searchField: 'Pesquise por uma Task'
  },
  results: {
    emptyList: {
      title: 'Oops, você ainda não tem nenhuma tarefa cadastrada.'
    },
    emptyFilter: {
      title: 'Oops, nenhuma tarefa encontrada com o termo: {filter}'
    },
    emptyError: {
      title: 'Oops, o serviço está indisponível no momento.',
      resultText: 'Tente novamente em breve.',
      button: 'Recarregar'
    }
  },
  actions: {
    edit: 'Editar tarefa {title}',
    delete: 'Excluir tarefa {title}'
  },
  form,
  delete: deleteModal,
  alerts
}
