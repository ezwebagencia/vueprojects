type NavItemChildren = {
  title: string
  link: string
}
type GaEvent = {
  event?: string
  trigger?: string
  action: string
  category: string
  label: string
  value: number | string
  noninteraction?: boolean
}
type NavItem = {
  title: string
  icon: string
  link: string
  external?: boolean
  children?: [NavItemChildren]
  category?: string
  routeGroup?: string
  ga?: GaEvent
  exactPath?: boolean
}

export const useSidebar = (): NavItem[] => {
  return [
    {
      title: 'Tela Inicial',
      icon: '$places-home-outline',
      link: '/'
    },
    {
      title: 'Tarefas',
      icon: '$web-system-list-outline',
      link: '/tarefas'
    }
  ]
}
