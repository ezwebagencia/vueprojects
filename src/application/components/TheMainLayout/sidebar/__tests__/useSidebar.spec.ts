import { useSidebar } from '../'

describe('useSidebar function behave as expected', () => {
  const sidebar = useSidebar()
  it('should return an array with two links for navigation', () => {
    expect(sidebar.length).toBe(2)
    expect(sidebar[0].title).toBe('Tela Inicial')
    expect(sidebar[1].title).toBe('Tarefas')
  })
})
