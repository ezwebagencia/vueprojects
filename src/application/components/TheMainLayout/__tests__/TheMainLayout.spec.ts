import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import TheMainLayoutVue from '../TheMainLayout.vue'
import { Wrapper } from '@vue/test-utils'
import Vue from 'vue'

describe('Test if TheApp component behave as expected', () => {
  let wrapper: Wrapper<Vue>
  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, TheMainLayoutVue)
  })
  it('should render TheMainLayout component', () => {
    expect(wrapper.findComponent(TheMainLayoutVue).exists()).toBe(true)
  })

  it('should render a navMenu and main content', () => {
    expect(wrapper.findComponent({ name: 'LwNavMenu' }).exists()).toBe(true)
    expect(wrapper.findComponent({ name: 'LwMain' }).exists()).toBe(true)
  })
})
