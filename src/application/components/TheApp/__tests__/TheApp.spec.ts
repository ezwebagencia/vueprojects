import TestUtils from '@/application/utils/TestUtils'
import { MountEnum } from '@locaweb/lw-test-utils'
import TheApp from '../TheApp.vue'
import { Wrapper } from '@vue/test-utils'
import Vue from 'vue'

describe('Test if TheApp component behave as expected', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: Wrapper<Vue>
  beforeAll(() => {
    wrapper = TestUtils.mountTest(MountEnum.SHALLOW, TheApp)
  })
  it('should render TheApp component', () => {
    expect(wrapper.findComponent(TheApp).exists()).toBe(true)
  })

  it('should render a router-view', () => {
    expect(wrapper.findComponent({ name: 'router-view' }).exists()).toBe(true)
  })
})
