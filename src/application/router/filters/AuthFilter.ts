import { useAuthStore } from '@/application/store/auth'
import User from '@/core/auth/domain/User'
import { useShouldBeAuthenticated } from '@locaweb/lw-http-client'
import { Route } from 'vue-router'

const [shouldBeAuthenticate] = useShouldBeAuthenticated({
  baseURL: window.location.origin,
  authorizationCookieName: import.meta.env.VITE_AUTHORIZATION_COOKIE_NAME,
  loginFunction: (data: User) => useAuthStore().login(new User(data))
})

export const authFilter = async (to: Route, from: Route, next: (route?: string) => void) => {
  import.meta.env.MODE === 'development' && shouldBeAuthenticate(to, from, next)
  next()
}
