import { RouteConfig } from 'vue-router'
import TheMainLayoutVue from '../components/TheMainLayout/TheMainLayout.vue'
import { authFilter } from './filters/AuthFilter'
import TaskRoutesPath from '../view/task/router'
import DashboardRoutesPath from '../view/dashboard/router'
const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: TheMainLayoutVue,
    beforeEnter: authFilter,
    children: [...DashboardRoutesPath, ...TaskRoutesPath]
  }
]

export default routes
