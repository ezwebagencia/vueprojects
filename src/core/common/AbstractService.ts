import { httpClient } from '@/infrastructure/http/HttpClient'
import { Axios } from 'axios'
export default abstract class AbstractService {
  httpClient: Axios = httpClient
}
