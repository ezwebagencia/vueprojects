import { AxiosResponse } from 'axios'
import AbstractService from '@/core/common/AbstractService'
import { mountUrl } from '@locaweb/lw-http-client'
import { TaskError } from '../error/TaskError'
import Task from '../../domain/Task'
import { FindAllParams } from '../../domain/FindAllParams'

const BASE_URL = '/tasks'

const urls = {
  base: BASE_URL,
  resource: `${BASE_URL}/$id`
}

export type findAllResponse = {
  tasks: Task[]
  pagination: {
    page: number
    limit: number
    totalItens: number
    totalPages: number
  }
}

class TaskService extends AbstractService {
  async findAll(
    params: FindAllParams = new FindAllParams()
  ): Promise<AxiosResponse<findAllResponse, TaskError>> {
    try {
      return await this.httpClient.get(urls.base, { params: params?.toJson() })
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      throw new TaskError(e.response.status, e)
    }
  }
  async create(task: Task): Promise<AxiosResponse<Task>> {
    try {
      return await this.httpClient.post(urls.base, task)
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      throw new TaskError(e.response.status, e)
    }
  }
  async edit(task: Task): Promise<AxiosResponse<Task>> {
    try {
      return await this.httpClient.put(mountUrl(urls.resource, { id: task.id }), task)
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      throw new TaskError(e.response.status, e)
    }
  }
  async delete(task: Task): Promise<AxiosResponse<Task>> {
    try {
      return await this.httpClient.delete(mountUrl(urls.resource, { id: task.id }))
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      throw new TaskError(e.response.status, e)
    }
  }
}
export default new TaskService()
