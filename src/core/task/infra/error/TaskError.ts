import { ServerErrors } from '@vuelidate/core'
import { HttpError } from '@/common/error/HttpError'

export enum TaskErrorKind {
  'BAD_REQUEST' = 400,
  'SERVER_ERROR' = 500,
  'NOT_FOUND' = 404,
  'CONFLICT' = 409
}

const TaskErrorMessage: { [key: string]: string } = {
  BAD_REQUEST: 'Preenchimento inválido no formulário de tarefa.',
  SERVER_ERROR: 'Erro interno.',
  CONFLICT: 'Status da tarefa conflitante.',
  NOT_FOUND: 'Tarefa não encontrada.'
}

interface ITaskCause {
  response: {
    status: number
    data: {
      errors: ServerErrors
    }
  }
}

export class TaskError extends HttpError {
  errors: ServerErrors | undefined
  constructor(status: number, cause?: ITaskCause) {
    const kind = TaskErrorKind[status]
    const message: string = TaskErrorMessage[kind]

    super({
      status,
      kind,
      message,
      cause
    })

    this.errors = status === 400 ? cause?.response.data.errors : {}

    Object.setPrototypeOf(this, TaskError.prototype)
  }
}
