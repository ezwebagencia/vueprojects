import Task from '../domain/Task'
import TaskService from '../infra/service/TaskService'

export class SaveUseCase {
  async save(task: Task): Promise<Task> {
    const { data } = task.id ? await TaskService.edit(task) : await TaskService.create(task)
    return data
  }
}

export default new SaveUseCase()
