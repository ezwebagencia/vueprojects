import { SaveUseCase } from '@/core/task/usecases/SaveUseCase'
import { httpClient } from '@/infrastructure/http/HttpClient'
import Task from '../../domain/Task'

const saveUseCase = new SaveUseCase()

describe('Test if SaveUseCase use case behave as expected for create a new Task', () => {
  const task = new Task('title', 'description', false)

  it('should success if server create a task', async () => {
    httpClient.post = vi.fn().mockResolvedValue({ status: 201, data: task })
    const data = await saveUseCase.save(task)
    expect(data).toEqual(task)
  })

  it('should return server error if server fails to create a task', async () => {
    httpClient.post = vi.fn().mockRejectedValue({ response: { status: 500 } })
    await expect(async () => await saveUseCase.save(task)).rejects.toThrowError('Erro interno.')
  })

  it('should return bad request if user does not fulfill form as expected', async () => {
    httpClient.post = vi
      .fn()
      .mockRejectedValue({ response: { status: 400, data: { errors: { titile: 'blank' } } } })
    await expect(async () => await saveUseCase.save(task)).rejects.toThrowError(
      'Preenchimento inválido no formulário de tarefa.'
    )
  })
})

describe('Test if SaveUseCase use case behave as expected for update Task', () => {
  const task = new Task('title', 'description', false, 1)

  it('should success if server update a task', async () => {
    httpClient.put = vi.fn().mockResolvedValue({ status: 204, data: task })
    const data = await saveUseCase.save(task)
    expect(data).toEqual(task)
  })

  it('should return server error if server fails to create a task', async () => {
    httpClient.put = vi.fn().mockRejectedValue({ response: { status: 500 } })
    await expect(async () => await saveUseCase.save(task)).rejects.toThrowError('Erro interno.')
  })

  it('should return bad request if user does not fulfill form as expected', async () => {
    httpClient.put = vi
      .fn()
      .mockRejectedValue({ response: { status: 400, data: { errors: { titile: 'blank' } } } })
    await expect(async () => await saveUseCase.save(task)).rejects.toThrowError(
      'Preenchimento inválido no formulário de tarefa.'
    )
  })

  it('should return conflict when user try update a task if unexpected state', async () => {
    httpClient.put = vi.fn().mockRejectedValue({ response: { status: 409 } })
    await expect(async () => await saveUseCase.save(task)).rejects.toThrowError(
      'Status da tarefa conflitante.'
    )
  })

  it('should return not found when user try update a task that does not exist on server', async () => {
    httpClient.put = vi.fn().mockRejectedValue({ response: { status: 404 } })
    await expect(async () => await saveUseCase.save(task)).rejects.toThrowError(
      'Tarefa não encontrada.'
    )
  })
})
