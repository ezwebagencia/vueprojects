import { DeleteUseCase } from '@/core/task/usecases/DeleteUseCase'
import { httpClient } from '@/infrastructure/http/HttpClient'
import Task from '../../domain/Task'

describe('Test if DeleteUseCase use case behave as expected', () => {
  const deleteUseCase = new DeleteUseCase()
  it('should return success if server delete task', async () => {
    httpClient.delete = vi.fn().mockResolvedValue({ status: 204 })
    const result = await deleteUseCase.delete(new Task())
    expect(result).toBeTruthy()
  })

  it('should return server error when server fails to delete a task', async () => {
    httpClient.delete = vi.fn().mockRejectedValue({ response: { status: 500 } })
    await expect(async () => await deleteUseCase.delete(new Task())).rejects.toThrowError(
      'Erro interno.'
    )
  })

  it('should return conflict error when server fails to delete a task because state is done', async () => {
    httpClient.delete = vi.fn().mockRejectedValue({ response: { status: 409 } })
    await expect(async () => await deleteUseCase.delete(new Task())).rejects.toThrowError(
      'Status da tarefa conflitante.'
    )
  })

  it('should validate if task can be deleted', () => {
    const task = new Task('title', 'description', false, 1)
    expect(deleteUseCase.validateDelete('lorem', task)).toBeFalsy()
    expect(deleteUseCase.validateDelete('title', task)).toBeTruthy()
  })
})
