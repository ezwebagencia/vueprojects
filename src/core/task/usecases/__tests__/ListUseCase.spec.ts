import { httpClient } from '@/infrastructure/http/HttpClient'
import { ListUseCase } from '../ListUseCase'

describe('Test if ListUseCase use case behave as expected', () => {
  const listUseCase = new ListUseCase()
  it('should return 200 and success if paginated tasks', async () => {
    const tasksPaginated = {
      tasks: [
        {
          id: 1,
          title: 'lorem ipsum',
          description: 'lorem ipsum'
        }
      ],
      pagination: {
        page: 1,
        limit: 10,
        totalItens: 1,
        totalPages: 1
      }
    }
    httpClient.get = vi.fn().mockResolvedValue({ data: { tasksPaginated }, status: 200 })
    const data = await listUseCase.listAll()
    expect(data).toEqual({ tasksPaginated })
  })

  it('should return a TaskError when user trying to list tasks and server has a problem', async () => {
    httpClient.get = vi.fn().mockRejectedValue({ response: { status: 500 } })
    await expect(async () => await listUseCase.listAll()).rejects.toThrowError('Erro interno.')
  })
})
