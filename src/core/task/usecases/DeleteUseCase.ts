import Task from '../domain/Task'
import TaskService from '../infra/service/TaskService'

export class DeleteUseCase {
  validateDelete(taskTitle: string, task: Task) {
    return taskTitle === task.title && !task.done
  }
  async delete(task: Task): Promise<boolean> {
    await TaskService.delete(task)
    return true
  }
}

export default new DeleteUseCase()
