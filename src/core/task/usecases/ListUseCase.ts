import { FindAllParams } from '../domain/FindAllParams'
import TaskService, { findAllResponse } from '../infra/service/TaskService'

export class ListUseCase {
  async listAll(params?: FindAllParams): Promise<findAllResponse> {
    const { data } = await TaskService.findAll(params)
    return data
  }
}

export default new ListUseCase()
