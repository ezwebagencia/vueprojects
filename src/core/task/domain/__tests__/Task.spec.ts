import Task from '../Task'

describe('Test if Task entity behave as expected', () => {
  const taskMock = {
    title: 'Lorem ipsum',
    description: 'Lorem ipsum dolor sit amet'
  }

  const task = new Task(...Object.values(taskMock))

  it('should render a task without id and done status as false', () => {
    expect(task).toEqual({ ...taskMock, done: false })
  })

  it('should return a clone of task', () => {
    expect(task.clone()).toEqual(task)
    expect(task.clone()).not.toBe({ ...task, id: 1 })
  })

  it('should return if task can be deleted if status done is false', () => {
    expect(task.canDelete()).toBeTruthy()
    expect(new Task('lorem', 'ipsum', true).canDelete()).toBeFalsy()
  })
})
