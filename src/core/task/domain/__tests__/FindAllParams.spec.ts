import { FindAllParams } from '../FindAllParams'

describe('Test if FindAllParams entity behave as expected', () => {
  const pagination = {
    page: 1,
    limit: 10
  }
  it('should return a FindAllParams only with pagination data', () => {
    expect(pagination).toEqual(new FindAllParams().toJson())
  })

  it('should return a FindAllParams with filter', () => {
    const filter = {
      filter: 'Lorem ipsum',
      ...pagination
    }
    expect(filter).toEqual(new FindAllParams('Lorem ipsum').toJson())
  })
})
