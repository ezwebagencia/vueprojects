import { PaginationType } from '@/common/domain/types'

export class FindAllParams {
  filter: string
  pagination: PaginationType

  constructor(filter?: string, pagination?: PaginationType) {
    this.filter = filter ?? ''
    this.pagination = pagination ?? {
      page: 1,
      limit: 10
    }
  }

  toJson() {
    return {
      ...(this.filter && { filter: this.filter }),
      ...this.pagination
    }
  }
}
