export interface ITask {
  id?: number
  title: string
  description: string
  done: boolean
}
export default class Task implements ITask {
  constructor(
    public title: string = '',
    readonly description: string = '',
    public done: boolean = false,
    public id?: number
  ) {}

  canDelete(): boolean {
    return !this.done
  }

  clone(): ITask {
    return new Task(this.title, this.description, this.done, this.id)
  }
}
