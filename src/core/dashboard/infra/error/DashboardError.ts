import { HttpError } from '@/common/error/HttpError'

export enum DashboardErrorKind {
  'SERVER_ERROR' = 500
}

const DashboardErrorMessage: { [key: string]: string } = {
  SERVER_ERROR: 'Erro interno durante a coleta dos status da tarefa'
}

export class DashboardError extends HttpError {
  constructor(status: number, cause?: unknown) {
    const kind = DashboardErrorKind[status]
    const message: string = DashboardErrorMessage[kind]

    super({
      status,
      kind,
      message,
      cause
    })

    Object.setPrototypeOf(this, DashboardError.prototype)
  }
}
