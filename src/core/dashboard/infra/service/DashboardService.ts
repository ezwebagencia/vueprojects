import AbstractService from '@/core/common/AbstractService'
import { IReport } from '../../domain/Report'
import { AxiosResponse } from 'axios'
import { DashboardError } from '../error/DashboardError'

const BASE_URL = '/report'

class DashboardService extends AbstractService {
  async getReport(): Promise<AxiosResponse<IReport>> {
    try {
      return await this.httpClient.get(BASE_URL)
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      throw new DashboardError(e.response.status, e)
    }
  }
}
export default new DashboardService()
