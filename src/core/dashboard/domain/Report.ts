import Task from '@/core/task/domain/Task'

export interface Summary {
  completed: number
  pending: number
  total: number
}
export interface IReport {
  summary?: Summary
  lastTasks?: Task[]
}
export default class Report implements IReport {
  constructor(public summary?: Summary, public lastTasks?: Task[]) {}
}
