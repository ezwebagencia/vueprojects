import DashboardService from '../infra/service/DashboardService'
import TaskService from '@/core/task/infra/service/TaskService'
import Report from '../domain/Report'
import { FindAllParams } from '@/core/task/domain/FindAllParams'
export class DashboardUseCase {
  async getReport() {
    const result = await Promise.allSettled([
      DashboardService.getReport(),
      TaskService.findAll(new FindAllParams())
    ])
    return new Report(result[0]?.value?.data.summary, result[1]?.value?.data.tasks.slice(0, 5))
  }
}

export default new DashboardUseCase()
