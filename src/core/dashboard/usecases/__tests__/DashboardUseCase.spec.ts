import TaskService from '@/core/task/infra/service/TaskService'
import { DashboardUseCase } from '../DashboardUseCase'
import { httpClient } from '@/infrastructure/http/HttpClient'
import tasks from './mocks/tasks'
import summary from './mocks/summary'
import Report from '../../domain/Report'

const dashboardUseCase = new DashboardUseCase()

describe('Test if DashboardUseCase use case behave as expected', () => {
  it('should return a report with summary and tasks', async () => {
    TaskService.findAll = vi.fn().mockResolvedValue({ status: 200, data: { tasks } })
    httpClient.get = vi.fn().mockResolvedValue({ status: 201, data: { summary } })
    const report = await dashboardUseCase.getReport()
    expect(report).toEqual({ summary, lastTasks: tasks })
  })

  it('should return a report without summary if server fails to get summary', async () => {
    TaskService.findAll = vi.fn().mockResolvedValue({ status: 200, data: { tasks } })
    httpClient.get = vi.fn().mockRejectedValue({ response: { status: 500 } })
    const report = await dashboardUseCase.getReport()
    expect(report).toEqual({ lastTasks: tasks })
  })

  it('should return a report without lastTasks if server fails to get summary', async () => {
    TaskService.findAll = vi.fn().mockRejectedValue({ response: { status: 500 } })
    httpClient.get = vi.fn().mockResolvedValue({ status: 201, data: { summary } })
    const report = await dashboardUseCase.getReport()
    expect(report).toEqual({ summary })
  })

  it('should return a report without data if server fails to get summary', async () => {
    TaskService.findAll = vi.fn().mockRejectedValue({ response: { status: 500 } })
    httpClient.get = vi.fn().mockRejectedValue({ response: { status: 500 } })
    const report = await dashboardUseCase.getReport()
    expect(report).toEqual(new Report())
  })
})
