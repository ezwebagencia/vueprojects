import User from '../User'

describe('Test if User entity behave as expected', () => {
  const userMock = {
    username: 'lorem',
    fullName: 'Lorem Ipsum',
    email: 'loremipsum@gmaimx.om'
  }

  it('should return a User', () => {
    expect(userMock).toEqual(new User(userMock))
  })

  it('should return a UserInfo with fullName and username as additionalInfo', () => {
    expect({
      name: userMock.fullName,
      aditionalInfo: userMock.username
    }).toEqual(new User(userMock).userInfo)
  })
})
