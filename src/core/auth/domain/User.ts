export interface IAuthUser {
  username: string
  fullName: string
  email: string
}

export type UserInfo = {
  name: string
  aditionalInfo: string
}
export default class User implements IAuthUser {
  username: string
  fullName: string
  email: string

  constructor({ username, fullName, email }: IAuthUser) {
    this.username = username
    this.fullName = fullName
    this.email = email
  }

  get userInfo(): UserInfo {
    return {
      name: this.fullName,
      aditionalInfo: this.username
    }
  }
}
