import { AxiosResponse } from 'axios'
import { User } from '../../domain/User'
import { httpClient } from '../../../../infrastructure/http/HttpClient'
import { HttpError } from '@/common/error/HttpError'
import { AuthenticationError } from '../error/AuthenticationError'

const urls = {
  login: '/login'
}
export default {
  async login(user: User): Promise<AxiosResponse<User, HttpError>> {
    try {
      return await httpClient.post(urls.login, user)
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (e: any) {
      throw new AuthenticationError(e.response.status as number, e)
    }
  }
}
