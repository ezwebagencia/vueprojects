import { HttpError } from '@/common/error/HttpError'

enum AuthenticationErrorKind {
  'BAD_REQUEST' = 400,
  'SERVER_ERROR' = 500,
  'UNAUTHORIZED' = 401,
  'FORBIDDEN' = 403
}

const AuthenticationErrorMessage: { [key: string]: string } = {
  BAD_REQUEST: 'Preenchimento inválido do formulário de login.',
  SERVER_ERROR: 'Falha da api durante o login.',
  UNAUTHORIZED: 'Falha na autenticação do usuário.',
  FORBIDDEN: 'Usuário efetou login, porém não possui permissão para seguir adiante.'
}

export class AuthenticationError extends HttpError {
  constructor(status: number, cause?: unknown) {
    const kind = AuthenticationErrorKind[status]
    const message: string = AuthenticationErrorMessage[kind]
    super({
      status,
      kind,
      message,
      cause
    })
    Object.setPrototypeOf(this, AuthenticationError.prototype)
  }
}
