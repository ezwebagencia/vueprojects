import { AxiosResponse } from 'axios'
import User from './../domain/User'
import { HttpError } from '@/common/error/HttpError'
import AuthService from '@/core/auth/infra/service/AuthService'

export interface ILogin {
  execute: (user: User) => Promise<AxiosResponse<User, HttpError>>
}

export class LoginUseCase implements ILogin {
  execute(user: User) {
    return AuthService.login(user)
  }
}

export default new LoginUseCase()
