import User from '../../domain/User'
import { LoginUseCase } from '../LoginUseCase'
import { httpClient } from '@/infrastructure/http/HttpClient'

describe('Test if LoginUseCase use case behave as expected', () => {
  const user: User = new User({
    username: 'loremipsum',
    fullName: 'lorem ipsum',
    email: 'teste@gmail.com'
  })
  const login = new LoginUseCase()
  it('should return 200 and success if users login', async () => {
    httpClient.post = vi.fn().mockResolvedValue({ data: { ...user }, status: 200 })
    const { data } = await login.execute(user)
    expect(data).toEqual(user)
  })

  it('should return a authentication error when user trying to login', async () => {
    httpClient.post = vi.fn().mockRejectedValue({ response: { status: 400 } })
    await expect(async () => await login.execute(user)).rejects.toThrowError(
      'Preenchimento inválido do formulário de login.'
    )
  })

  it('should return a authentication error when user trying to login and server has a problem', async () => {
    httpClient.post = vi.fn().mockRejectedValue({ response: { status: 500 } })
    await expect(async () => await login.execute(user)).rejects.toThrowError(
      'Falha da api durante o login.'
    )
  })
})
