import { Axios } from 'axios'
import { createClient } from '@locaweb/lw-http-client'

export const httpClient: Axios = createClient({
  baseURL: window.location.origin,
  authorizationCookieName: 'template-api',
  env: process.env.NODE_ENV
})
