import { setupWorker } from 'msw'
import { handlers } from './handlers'

const worker = import.meta.env.MODE === 'test' ? {} : setupWorker(...handlers)

export { worker }
