import TaskHandlers from './task/TaskHandlers'
import AuthHandlers from './auth/AuthHandlers'
import DashboardHandlers from './dashboard/DashboardHandlers'
export const handlers = [...AuthHandlers, ...DashboardHandlers, ...TaskHandlers]
