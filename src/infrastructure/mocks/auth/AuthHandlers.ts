import { rest } from 'msw'

export default [
  rest.get('/auth/token', async (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        username: 'loremipsum',
        email: 'lorem@gmail.com',
        fullName: 'Lorem Ipsum'
      })
    )
  })
]
