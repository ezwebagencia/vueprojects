export default [
  ...Array(10)
    .fill('')
    .map((_v, index) => ({
      id: index + 1,
      title: `Lorem ${index + 2}`,
      description: `lorem ${index + 2}`,
      done: !Math.round(Math.random())
    })),
  {
    id: 11,
    title: 'conflict',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
    done: false
  },
  {
    id: 12,
    title: 'server error',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
    done: false
  },
  {
    id: 13,
    title: 'Lorem ipsum dolor sit amet',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
    done: false
  }
]
