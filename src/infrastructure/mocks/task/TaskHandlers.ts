import { rest } from 'msw'
import tasks from './mocks/tasks'

const PREFIX_URL = '/tasks'

let state = tasks.toSorted((a, b) => b.id - a.id)
export default [
  rest.get(PREFIX_URL, async (req, res, ctx) => {
    const page: number = Number(req.url.searchParams.get('page') ?? 1) - 1
    const limit = Number(req.url.searchParams.get('limit') ?? 10)
    const filter = req.url.searchParams.get('filter')
    const start = page * limit
    const end = start + limit
    const filteredMocks = state.filter(
      (task) =>
        !filter ||
        task.title.toLocaleLowerCase().startsWith(filter) ||
        task.description.toLocaleLowerCase().startsWith(filter.toLocaleLowerCase())
    )
    const response = {
      tasks: filteredMocks.slice(start, end),
      pagination: {
        page: page + 1,
        limit,
        totalItens: filteredMocks.length,
        totalPages: Math.ceil(filteredMocks.length / limit)
      }
    }
    return res(ctx.status(200), ctx.json(response), ctx.delay(2000))
  }),
  rest.post(PREFIX_URL, async (req, res, ctx) => {
    const body = await req.json()
    if (body.title === 'client error') {
      return res(
        ctx.status(400),
        ctx.json({
          errors: {
            title: 'blank',
            description: 'invalid'
          }
        })
      )
    }
    if (body.title === 'server error') {
      return res(ctx.status(500))
    }
    body.id = new Date().getTime()
    state.push(body)
    return res(ctx.status(201), ctx.json(body))
  }),
  rest.put(`${PREFIX_URL}/:taskId`, async (req, res, ctx) => {
    const { taskId } = req.params
    const body = await req.json()
    const task = state.find((task) => task.id === Number(taskId))
    if (!task || body.title === 'deleted') {
      return res(ctx.status(404))
    }
    if (body.title === 'conflict') {
      return res(ctx.status(409))
    }
    if (body.title === 'client error') {
      return res(
        ctx.status(400),
        ctx.json({
          errors: {
            title: 'blank',
            description: 'invalid'
          }
        })
      )
    }
    if (body.title === 'server error') {
      return res(ctx.status(500))
    }
    state[state.findIndex((task) => task.id === body.id)] = body
    return res(ctx.status(204))
  }),
  rest.delete(`${PREFIX_URL}/:taskId`, async (req, res, ctx) => {
    const { taskId } = req.params
    const task = state.find((task) => task.id === Number(taskId))
    if (!task || task.title === 'Lorem ipsum dolor sit amet') {
      return res(ctx.status(404))
    }
    if (task.title === 'conflict') {
      return res(ctx.status(409))
    }
    if (task.title === 'server error') {
      return res(ctx.status(500))
    }
    state = state.filter((tsk) => tsk.id !== task?.id)
    return res(ctx.status(204))
  })
]
