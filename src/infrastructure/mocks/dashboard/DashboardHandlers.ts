import { rest } from 'msw'
import summary from './mocks/summary'
export default [
  rest.get('/report', async (_res, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        summary
      })
    )
  })
]
