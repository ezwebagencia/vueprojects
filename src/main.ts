import Vue from 'vue'
import '@locaweb/design-system-vue/dist/be-online.css'
import TheApp from './application/components/TheApp/TheApp.vue'
import ConfigPlugin from './application/config'
import DesignSystem from '@locaweb/design-system-vue'
import { createPinia } from 'pinia'

Vue.config.productionTip = false
Vue.use(ConfigPlugin)

new Vue({
  pinia: createPinia(),
  ...DesignSystem,
  render: (h) => h(TheApp)
}).$mount('#app')
