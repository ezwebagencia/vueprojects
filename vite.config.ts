import { fileURLToPath, URL } from 'node:url'
import { VitePWA } from 'vite-plugin-pwa'
import DesignSystemBuilder from '@locaweb/design-system-builder'

export default DesignSystemBuilder({
  base: '/app',
  server: {
    port: 8001,
    host: true,
    strictPort: true
  },
  plugins: () => {
    return [
      ...([
        VitePWA({
          registerType: 'autoUpdate',
          workbox: {
            clientsClaim: true,
            skipWaiting: true,
            runtimeCaching: [
              {
                urlPattern: /(.*)be-online-(.*).js/,
                handler: 'NetworkOnly'
              },
              {
                // eslint-disable-next-line prefer-regex-literals
                urlPattern: new RegExp('(.*).js'),
                handler: 'NetworkOnly'
              },
              {
                // eslint-disable-next-line prefer-regex-literals
                urlPattern: new RegExp('https://fonts.'),
                handler: 'CacheFirst',
                options: {
                  cacheName: 'fonts'
                }
              }
            ]
          }
        })
      ])
    ]
  },
  alias: {
    '@task': fileURLToPath(new URL('./src/application/view/task', import.meta.url)),
    '@dashboard': fileURLToPath(new URL('./src/application/view/dashboard', import.meta.url))
  }
})
