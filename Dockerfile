FROM node:18-alpine AS builder

WORKDIR /app

COPY package.json ./

COPY .npmrc .npmrc

RUN yarn config set //registry.npmjs.org/:_authToken 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

RUN yarn install

COPY . .

RUN yarn build

FROM nginx:1.21.1-alpine

COPY --from=builder /app/dist /usr/share/nginx/html

COPY ./server/nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]